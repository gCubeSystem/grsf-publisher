This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Global Record of Stocks and Fisheries (GRSF) Publisher

## [v1.2.0]

- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]


## [v1.1.1]

- Added 'Jurisdictional Distribution' and 'Fishing Effort' #27796
- Removed thousands of separators from Reference years #27886
- Added 'Map Preview' support #27843
- Forced 'searchable' to 'True' to solve statistic issue #27922
- Added Content-Location HTTP Header #27785
- Fixed Location HTTP Header #27785
- Fixed bug which removed dots from tags #27972
- Extra '-' at the beginning of timeseries metadata has been fixed #27972


## [v1.1.0]

- Accepted input has been changed see #25008 
- catalogue-core library is used to interact with Ckan #27118
- Added missing keycloak-client library to set it to provided


## [v1.0.0]

- First Version

