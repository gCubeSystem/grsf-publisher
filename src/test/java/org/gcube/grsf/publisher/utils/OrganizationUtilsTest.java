package org.gcube.grsf.publisher.utils;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.grsf.publisher.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class OrganizationUtilsTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(OrganizationUtilsTest.class);
	
	@Test
	public void getOrganizations() {
		OrganizationUtils organizationUtils = OrganizationUtils.getInstance();
		Map<String, String> orgs = organizationUtils.getOrganizations();
		logger.debug("{}", orgs);
		JsonNode jsonNode = organizationUtils.getOrganizationJsonNode();
		logger.debug("{}", jsonNode);
	}
	
}
