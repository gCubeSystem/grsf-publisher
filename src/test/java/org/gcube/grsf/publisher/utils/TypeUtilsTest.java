package org.gcube.grsf.publisher.utils;

import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeUtilsTest {

	private static Logger logger = LoggerFactory.getLogger(TypeUtilsTest.class);
	
	@Test
	public void getTypes() {
		TypeUtils typeUtils = TypeUtils.getInstance();
		Map<String, Set<String>> types = typeUtils.getTypes();
		logger.debug("{}", types);
		JsonNode jsonNode = typeUtils.getTypesJsonNode();				
		logger.debug("{}", jsonNode);
	}
	
}
