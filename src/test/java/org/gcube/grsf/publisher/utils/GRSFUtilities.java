package org.gcube.grsf.publisher.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.persistence.ckan.CKANGroup;
import org.gcube.gcat.persistence.ckan.CKANOrganization;
import org.gcube.gcat.persistence.ckan.CKANUser;
import org.gcube.gcat.persistence.ckan.CKANUtility;
import org.gcube.grsf.publisher.ContextTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFUtilities extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(GRSFUtilities.class);
	
	public static final Map<String,String> SOURCE_TO_ID;
	public static final Map<String,String> ID_TO_SOURCE;
	
	static {
		ID_TO_SOURCE = new HashMap<>();
		ID_TO_SOURCE.put("ram", "RAMLDB");
		ID_TO_SOURCE.put("sdg", "FAO SDG 14.4.1 Questionnaire");
		
		SOURCE_TO_ID = new HashMap<>();
		for(String id : ID_TO_SOURCE.keySet()) {
			SOURCE_TO_ID.put(ID_TO_SOURCE.get(id), id);
		}
	}
	
	@Before
	public void before() throws Exception {
//		ContextTest.setContextByName(PREPROD_GRSF_PRE);
//		ContextTest.setContextByName(PRODUCTION_GRSF);
//		ContextTest.setContextByName(PRODUCTION_GRSF_ADMIN);
//		ContextTest.setContextByName(PRODUCTION_GRSF_PRE);
//		logger.debug(SecretManagerProvider.instance.get().getUser().getUsername());
//		logger.debug(SecretManagerProvider.instance.get().getContext());
	}
	
	private void create(Set<String> createdGroup, Map<String, String> groups, String name) throws JsonProcessingException, IOException {
		if(createdGroup.contains(name)) {
			return;
		}
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		CKANGroup ckanGroupToCreate = new CKANGroup();
		ckanGroupToCreate.setApiKey(sysAdminAPI);
		String id = getGroupId(name);
		ckanGroupToCreate.setName(id);
		ckanGroupToCreate.setTitle(name);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(groups.get(name));
		ArrayNode array = (ArrayNode) jsonNode.get("groups");
		for(JsonNode node : array) {
			String parentName = node.get("name").asText();
			if(!createdGroup.contains(parentName)) {
				create(createdGroup, groups, parentName);
			}
		}
		ckanGroupToCreate.create(groups.get(name));
		createdGroup.add(name);
	}
	
	public List<String> listGroup(boolean fromCKAN) throws Exception {
		String[] groupArray = null;
		if(fromCKAN) {
			CKANGroup group = new CKANGroup();
			String groups = group.list(1000, 0);
			ObjectMapper objectMapper = new ObjectMapper();
			groupArray = objectMapper.readValue(groups, String[].class);
		}else {
			groupArray = new String[] {
					"Abundance Level",
					"Abundance Level (FIRMS Standard)",
					"Assessment Method",
					"Biomass",
					"Catch",
					"FAO SDG 14.4.1 Questionnaire",
					"FAO Stock Status Category",
					"FIRMS",
					"FishSource",
					"Fishery",
					"Fishing Effort",
					"Fishing Pressure",
					"Fishing Pressure (FIRMS Standard)",
					"GRSF",
					"GRSF SDG Flag",
					"GRSF Traceability Flag",
					"Landing",
					"Legacy",
					"RAM",
					"Scientific Advice",
					"State and Trend",
					"Stock",
					"Traceability Unit"
			};
			//groupArray = new String[] {};
		}
		return Arrays.asList(groupArray);
	}
	
	@Ignore
	@Test
	public void createGRSFGroups() throws ObjectNotFound, Exception {
		
		// Reading groups from a CKAN related to a VRE
		ContextTest.setContextByName(PRODUCTION_GRSF);
		
		String key = CKANUtility.getSysAdminAPI();
		
		List<String> groupNames = listGroup(true);
		
		Map<String, String> groups = new HashMap<>();
		for(String name : groupNames) {
			CKANGroup ckanGroup = new CKANGroup();
			ckanGroup.setApiKey(key);
			ckanGroup.setName(name);
			String read = ckanGroup.read();
			groups.put(name, read);
		}
		
		Set<String> createdGroup = new HashSet<>();
		
		// Creating groups in another related to another VRE
		ContextTest.setContextByName(PREPROD_GRSF_PRE);
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		
		for(String name : groupNames) {
			create(createdGroup, groups, name);
		}
		
		String[] usernames = getUsers();
		boolean fromCKAN = true;
		
		Set<String> orgs = getOrganizations().keySet();
		for(String username : usernames) {
			
			CKANUser ckanUser = new CKANUser();
			ckanUser.setApiKey(sysAdminAPI);
			ckanUser.setName(username);
			
			for(String groupName : groupNames) {
				if(!fromCKAN) {
					groupName = getGroupId(groupName);
				}
				ckanUser = addUserToGroup(ckanUser, groupName);
			}
			
			for(String org : orgs) {
				ckanUser = addUserToOrganization(ckanUser, org);
			}
			
		}
		
	}
	
	public static String getGroupNameOnCkan(String originalName){
		if(originalName == null) {
			throw new IllegalArgumentException("original Name cannot be null");
		}
		if(SOURCE_TO_ID.containsKey(originalName)) {
			return SOURCE_TO_ID.get(originalName);
		}
		String modified = originalName.replaceAll("\\(", "");
		modified = modified.replaceAll("\\)", "");
		modified = modified.trim().toLowerCase().replaceAll("[^A-Za-z0-9-]", "-");
		if(modified.startsWith("-")) {
			modified = modified.substring(1);
		}
		if(modified.endsWith("-")) {
			modified = modified.substring(0, modified.length() -1);
		}
		return modified;
	}
	
	public static final String GROUP_SUFFIX = "-group";
	
	public static String getGroupId(String name) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(getGroupNameOnCkan(name));
		if(!name.endsWith(GROUP_SUFFIX)) {
			stringBuffer.append(GROUP_SUFFIX);
		}
		return stringBuffer.toString();
	}
	
	@Ignore
	@Test
	public void testGroupName() throws Exception {
		List<String> groupNames = listGroup(false);
		for(String name : groupNames) {
			String ckanGroupName = getGroupId(name);
			String groupName = CKANGroup.fromGroupTitleToName(name) + GROUP_SUFFIX;
			logger.info("{} -> {}", name, ckanGroupName, groupName);
			Assert.assertTrue(ckanGroupName.compareTo(groupName)==0);
		}
	}
	
	@Ignore
	@Test
	public void createNewGRSFGroups() throws ObjectNotFound, Exception {
		ContextTest.setContextByName(PREPROD_GRSF_PRE);
		
		String[] usernames = getUsers();
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		
		List<String> groupNames = listGroup(false);
		ObjectMapper objectMapper = new ObjectMapper();
		for(String name : groupNames) {
			CKANGroup ckanGroupToCreate = new CKANGroup();
			ckanGroupToCreate.setApiKey(sysAdminAPI);
			String id = getGroupId(name);
			ckanGroupToCreate.setName(id);
			ObjectNode node =  objectMapper.createObjectNode();
			node.put("display_name", name);
			node.put("title", name);
			node.put("name", id);
			String json = objectMapper.writeValueAsString(node);
			logger.info(json);
			ckanGroupToCreate.create(json);
//			for(String username : usernames) {
//				addUserToGroup(username, name, sysAdminAPI);
//			}
		}
	}
	
	@Test
	public void updateGroups() throws ObjectNotFound, Exception {
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		List<String> groupNames = listGroup(false);
		ObjectMapper objectMapper = new ObjectMapper();
		for(String name : groupNames) {
			CKANGroup ckanGroup = new CKANGroup();
			ckanGroup.setApiKey(sysAdminAPI);
			ckanGroup.setName(getGroupId(name));
			String read = ckanGroup.read();
			
			ObjectNode node =  (ObjectNode) objectMapper.readTree(read);
			node.put("display_name", name);
			node.put("title", name);
			
			String json = objectMapper.writeValueAsString(node);
			logger.info(json);
			ckanGroup.update(json);	
		}
	}
	
	@Ignore
	@Test
	public void deleteGRSFGroups() throws ObjectNotFound, Exception {
		ContextTest.setContextByName(PREPROD_GRSF_PRE);
		
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		boolean fromCKAN = true;
		List<String> groupNames = listGroup(fromCKAN);
		logger.debug(SecretManagerProvider.instance.get().getUser().getUsername());
		logger.debug(SecretManagerProvider.instance.get().getContext());
		
		for(String name : groupNames) {
			CKANGroup ckanGroupToPurge = new CKANGroup();
			ckanGroupToPurge.setApiKey(sysAdminAPI);
			if(!fromCKAN) {
				name = getGroupId(name);
			}
			ckanGroupToPurge.setName(name);
			
			try {
//				ckanGroupToPurge.purge();
			}catch (Exception e) {
				logger.error("Error while purging group with name {}", name, e);
			}
		}
	}
	
	protected void addUserToGroup(String username, String groupName) {
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		addUserToGroup(username, groupName, sysAdminAPI);
	}
	
	protected void addUserToGroup(String username, String groupName, String sysAdminAPI) {
		CKANUser ckanUser = new CKANUser();
		if(sysAdminAPI!=null) {
			ckanUser.setApiKey(sysAdminAPI);
		}
		ckanUser.setName(username);
		addUserToGroup(ckanUser, groupName);
	}
	
	protected CKANUser addUserToGroup(CKANUser ckanUser, String groupName) {
		ckanUser.addToGroup(groupName);
		return ckanUser;
	}
	
	protected CKANUser addUserToOrganization(CKANUser ckanUser, String orgName) {
		ckanUser.addUserToOrganization(orgName, ckanUser.getName(), "member");
		return ckanUser;
	}
	
	protected String[] getUsers() {
		String[] usernames = new String[] { "grsf_publisher", "luca_frosini" };
		return usernames;
	}
	
	@Ignore
	@Test
	public void associateUserToAllCKANGroupsAndOrganization() throws ObjectNotFound, Exception {
		ContextTest.setContextByName(PREPROD_GRSF_PRE);
		String[] usernames = getUsers();
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		boolean fromCKAN = true;
		
		List<String> groupNames = listGroup(fromCKAN);
		
		Set<String> orgs = getOrganizations().keySet();
		for(String username : usernames) {
			
			CKANUser ckanUser = new CKANUser();
			ckanUser.setApiKey(sysAdminAPI);
			ckanUser.setName(username);
			
			for(String groupName : groupNames) {
				if(!fromCKAN) {
					groupName = getGroupId(groupName);
				}
				ckanUser = addUserToGroup(ckanUser, groupName);
			}
			
			for(String org : orgs) {
				ckanUser = addUserToOrganization(ckanUser, org);
			}
			
		}

	}
	
	@Ignore
	@Test
	public void manageOrganizations() throws Exception {
		ContextTest.setContextByName(PREPROD_GRSF_PRE);
		String sysAdminAPI = CKANUtility.getSysAdminAPI();
		ObjectMapper objectMapper = new ObjectMapper();
		
		boolean create = true; // false means purge
		
		Map<String, String> organizations = getOrganizations();
		for(String org : organizations.keySet()) {
			CKANOrganization ckanOrganization = new CKANOrganization();
			ckanOrganization.setApiKey(sysAdminAPI);
			ckanOrganization.setName(org.toLowerCase());
			try {
				if(create) {
					ObjectNode node = objectMapper.createObjectNode();
					String fancyName = organizations.get(org);
					node.put("display_name", fancyName);
					node.put("title", fancyName);
					node.put("name", ckanOrganization.getName());
					String json = objectMapper.writeValueAsString(node);
					logger.info("Going to create organization {} : {}", ckanOrganization.getName(), json);
//					ckanOrganization.create(json);
					logger.info("Organization {} created successfully", ckanOrganization.getName());
				}else {
					logger.info("Going to purge organization {}", ckanOrganization.getName());
//					ckanOrganization.purge();
					logger.info("Organization {} purged successfully", ckanOrganization.getName());
				}
			}catch (Exception e) {
				logger.error("Error while {} organization {}", create ? "creating" : "purging", ckanOrganization.getName(), e);
			}
		}
		
	}
	
	private Map<String, String> getOrganizations() {
		return OrganizationUtils.getInstance().getOrganizations();
	}
	
}
