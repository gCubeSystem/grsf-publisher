package org.gcube.grsf.publisher.freemarker;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.Writer;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.utils.URIResolver;
import org.gcube.grsf.publisher.ContextTest;
import org.gcube.grsf.publisher.ckan.record.Record;
import org.gcube.grsf.publisher.configuration.GRSFCatalogueConfiguration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Template;

public class FreeMarkerTest {

	private static Logger logger = LoggerFactory.getLogger(FreeMarkerTest.class);

	public static final String GRSF_TYPE_PROPERTY = "grsf_type";
	public static final String SOURCE_PROPERTY = "source";
	
	protected ObjectMapper mapper;
	protected GRSFCatalogueConfiguration grsfCC;
	protected URIResolver uriResolver;
	protected boolean offline;
	
	public FreeMarkerTest() {
		offline = false;
		mapper = new ObjectMapper();
		try {
			ContextTest.setContextByName(ContextTest.DEVVRE);
			uriResolver = URIResolver.getInstance();
			grsfCC = (GRSFCatalogueConfiguration) CatalogueConfigurationFactory.getInstance();
		}catch (Throwable t) {
			// To test the template offline
			offline = true;
			grsfCC = new GRSFCatalogueConfiguration();
			grsfCC.setIncludeSensitive(true);
		}
	}
	
	public File getExampleDirectory(String type) throws Exception {
		File resourcesDirectory = getResourcesDirectory();
		return new File(resourcesDirectory, "examples/" + type);
	}
	
	protected File getResourcesDirectory() throws Exception {
		URL fileURL = FreeMarker.class.getClassLoader().getResource("config.properties");
		File logbackFile = new File(fileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return resourcesDirectory;
	}
	
	@Test
	public void testAllForCreateUpdate() throws Exception {
		testAll(false);
	}
	
	@Test
	public void testAllForPatchToo() throws Exception {
		testAll(true);
	}
	
	public void testAll(boolean patch) throws Exception {
		int maxTestRecords = 1000000000;
		int maxTestRecordsPerSource = 1000000;
		
		FreeMarker freeMarker = new FreeMarker();
				
		Template tsTemplate = freeMarker.getTemplate("timeseries.ftl");
		
		String[] types = new String[] { "Stock", "Fishery", "Traceability Unit" };
		// String[] types = new String[] {"Stock"};
		// String[] types = new String[] {"Fishery"};
		// String[] types = new String[] {"Traceability Unit"};
		
		Calendar start = Calendar.getInstance();
		
		int countRecord = 0;
		int countTimeSeries = 0;
		
		File outputDir = new File(getResourcesDirectory(), "output");
		outputDir.mkdir();
		
		FilenameFilter dirnameFilter = new FilenameFilter() {
	        @Override
	        public boolean accept(File dir, String name) {
	            File f = new File(dir, name);
	            return f.isDirectory();
	        }
	    };
	    
	    FilenameFilter filenameFilter = new FilenameFilter() {
	        @Override
	        public boolean accept(File dir, String name) {
	            return name.toLowerCase().endsWith(".json");
	        }
	    };
		
		for(String type : types) {
			if(countRecord >= maxTestRecords) {
				logger.info("The test has already elaborated {} records which is the max allowed (i.e. allowed {} records)", countRecord, maxTestRecords);
				return;
			}
			
			Template template = freeMarker.getTemplate(type + ".ftl");
			
			File examplesDir = getExampleDirectory(type);
			
			File outputTypeDir = new File(outputDir, type);
		    outputTypeDir.mkdir();
		    
		    
		    File[] sourceFiles = examplesDir.listFiles(dirnameFilter);
		    // File[] sourceFiles = new File[] {new File(examplesDir,"sdg")};
		    // File[] sourceFiles = new File[] { new File(examplesDir, "firms") };
		    
		    for(File source : sourceFiles) {
		    	if(countRecord >= maxTestRecords) {
					logger.info("The test has already elaborated {} records which is the max allowed (i.e. allowed {} records)", countRecord, maxTestRecords);
					return;
				}
		    	
		    	String sourceString = source.getName();
		    	
		    	File outputSourceDir = new File(outputTypeDir, sourceString);
		    	outputSourceDir.mkdir();
		    	
		    	File[] jsonFiles = source.listFiles(filenameFilter);
		    	// File[] jsonFiles = new File[] {new File(source,"31601d3d-6b0f-4b06-9afb-19d4cc6f7e12.json")};
		    	// File[] jsonFiles = new File[] {new File(source,"c75bf19c-37d4-3977-87f2-a8c4a3ecfc85.json")};
			    
		    	int countRecordPerSource = 0;
				
				for(File jsonFile : jsonFiles) {
					if(countRecord >= maxTestRecords) {
						logger.info("The test has already elaborated {} records which is the max allowed (i.e. allowed {} records)", countRecord, maxTestRecords);
						return;
					}
					
					if(countRecordPerSource >= maxTestRecordsPerSource) {
						logger.info("The test has already elaborated {} records for the source '{}' which is the max allowed (i.e. allowed {} records per source)", countRecordPerSource, sourceString, maxTestRecordsPerSource);
						break;
					}					
					
					mapper.readTree(jsonFile);
				    
				    @SuppressWarnings("unchecked")
					Map<String, Object> map = mapper.readValue(jsonFile, Map.class);
				    String grsfUUID = map.get(Record.GRSF_UUID_PROPERTY).toString();
				    
				    File recordOutputDir = new File(outputSourceDir, grsfUUID);
				    if(recordOutputDir.exists()) {
					    File[] filesToDelete = recordOutputDir.listFiles();
					    for(File fileToDelete : filesToDelete) {
					    	fileToDelete.delete();
					    }
					    recordOutputDir.delete();
				    }
				    recordOutputDir.mkdir();
					
				    ++countRecord;
				    ++countRecordPerSource;
				    
				    if(patch) {
				    	Set<String> keys = map.keySet();
				    	for(String key : keys) {
				    		switch (key) {
								case Record.GRSF_UUID_PROPERTY:
								case GRSF_TYPE_PROPERTY:
									continue;
	
								default:
									break;
							}
				    		
				    		Map<String, Object> mapForPatch = new HashMap<>();
				    		mapForPatch.put(key, map.get(key));
				    		mapForPatch.put(Record.GRSF_UUID_PROPERTY, grsfUUID);
				    		mapForPatch.put(Record.INCLUDE_SENSITIVE_TEMPLATE_PROPERTY_KEY, grsfCC.isIncludeSensitive());
				    		mapForPatch.put(Record.IS_PATCH_TEMPLATE_PROPERTY_KEY, patch);
				    		mapForPatch.put(GRSF_TYPE_PROPERTY, map.get(GRSF_TYPE_PROPERTY));
				    		mapForPatch.put(SOURCE_PROPERTY, sourceString);
				    		
				    		
				    		logger.info("Map created for key {} is {}", key, mapForPatch);
				    		
				    		File out = new File(recordOutputDir, key + ".json");
						    out.delete();
						    
						    generate(template, tsTemplate, out, type, mapForPatch);
						    
				    	}
				    	
				    }
				    
			    	String recordURL = "";
			    	if(!offline) {
			    		recordURL = uriResolver.getCatalogueItemURL(grsfUUID);
			    	}else {
			    		// This allow to test template if we are offline
			    		recordURL = "http://data.d4science.org/ctlg/GRSF/" + grsfUUID;
			    	}
					map.put(Record.RECORD_URL_TEMPLATE_PROPERTY_KEY, recordURL);
					map.put(Record.INCLUDE_SENSITIVE_TEMPLATE_PROPERTY_KEY, grsfCC.isIncludeSensitive());
					map.put(Record.IS_PATCH_TEMPLATE_PROPERTY_KEY, patch);
					map.put(SOURCE_PROPERTY, sourceString);
				    
				    logger.trace("Elaborating {} {} from file {}", sourceString, type, jsonFile.getName());
				    
				    File out = new File(recordOutputDir, jsonFile.getName());
				    out.delete();
				    
				    countTimeSeries += generate(template, tsTemplate, out, type, map);
				    
				}
		    }
		}
		
		Calendar end = Calendar.getInstance();
		long diff= end.getTimeInMillis() - start.getTimeInMillis();
		logger.info("Generated {} records and {} CSV files from TimeSeries in {} milliseconds (~{} seconds)", countRecord, countTimeSeries, diff, TimeUnit.MILLISECONDS.toSeconds(diff));
	}
	
	protected int generate(Template template, Template tsTemplate, File out, String type, Map<String, Object> map) throws Exception {
	    
	    Writer writer = new FileWriter(out);
        template.process(map, writer);
        writer.flush();
        writer.close();
        
        logger.trace("Template for type {} produced the result in file {}", type, out.getAbsolutePath());
        
        int countTimeSeries = 0;
        
        JsonNode node = mapper.readTree(out);
        ArrayNode timeseries = (ArrayNode) node.get("timeseries");
        for(JsonNode n : timeseries) {
        	String key = n.get("property").asText();
        	String fileName = n.get("filename").asText();
        	
        	logger.trace("Elaborating {} timeseries {} from file {}", type, key, out.getAbsolutePath());
        	
        	map.put("timeseries", map.get(key));
        	
        	File tsOut = new File(out.getParent(), fileName+".csv");
        	tsOut.delete();
		    
		    Writer tsWriter = new FileWriter(tsOut);
	        tsTemplate.process(map, tsWriter);
	        tsWriter.flush();
	        tsWriter.close();
	        
	        ++countTimeSeries;
	        
	        map.remove(Record.TIMESERIES_PROPERTY);
        }
        
        return countTimeSeries;
	}

	@Test
	public void testRegex() {
		Pattern pattern = Pattern.compile("[^\\s\\w-_]");
	    Matcher matcher = pattern.matcher("Code: NEP, Classification System: ASFIS, Scientific Name: Nephrops norvegicus");
//	    int matches = 0;
//	    while (matcher.find()) {
//	    	matches++;
//	    }
//	    
	    String replaced = matcher.replaceAll("");
	    logger.info("Replaced {}", replaced);
	}
}