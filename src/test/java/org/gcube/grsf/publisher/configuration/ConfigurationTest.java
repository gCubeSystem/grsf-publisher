package org.gcube.grsf.publisher.configuration;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.grsf.publisher.ContextTest;
import org.gcube.grsf.publisher.configuration.isproxies.impl.GRSFFacetBasedISConfigurationProxy;
import org.gcube.grsf.publisher.configuration.isproxies.impl.GRSFFacetBasedISConfigurationProxyFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ConfigurationTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationTest.class);
	
	@Test
	public void testFacetBasedConf() throws Exception {
		String context = SecretManagerProvider.instance.get().getContext();
		GRSFFacetBasedISConfigurationProxyFactory fbigcpf = new GRSFFacetBasedISConfigurationProxyFactory();
		GRSFFacetBasedISConfigurationProxy facetBasedISConfigurationProxy = fbigcpf.getInstance(context);
		facetBasedISConfigurationProxy.installQueryTemplate();
	}
	
	@Test
	public void testConfiguration() throws Exception {
		GRSFCatalogueConfiguration grsfCC = GRSFCatalogueConfigurationFactory.getInstance();
		String configuration = grsfCC.toJsonString();
		logger.info("Configuration in context {} is {}", grsfCC.getContext(), configuration);
	}
	
}
