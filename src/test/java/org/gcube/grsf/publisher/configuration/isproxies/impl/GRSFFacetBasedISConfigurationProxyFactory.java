package org.gcube.grsf.publisher.configuration.isproxies.impl;

import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFFacetBasedISConfigurationProxyFactory extends ISConfigurationProxyFactory<GRSFFacetBasedISConfigurationProxy> {

	public GRSFFacetBasedISConfigurationProxyFactory() {
		super();
	}

	@Override
	protected GRSFFacetBasedISConfigurationProxy newInstance(String context) {
		return new GRSFFacetBasedISConfigurationProxy(context);
	}

	
}
