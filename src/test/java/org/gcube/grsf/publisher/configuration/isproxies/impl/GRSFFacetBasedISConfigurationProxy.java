package org.gcube.grsf.publisher.configuration.isproxies.impl;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.configuration.isproxies.impl.FacetBasedISConfigurationProxy;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.grsf.publisher.configuration.GRSFCatalogueConfiguration;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFFacetBasedISConfigurationProxy extends FacetBasedISConfigurationProxy {

	public GRSFFacetBasedISConfigurationProxy(String context) {
		super(context);
		serviceName = "grsf-publisher";
	}
	
//	@Override
//	protected File getBaseDirectory(String directoryName) {
//		URL directoryURL = this.getClass().getClassLoader().getResource(directoryName);
//		File directory = new File(directoryURL.getPath());
//		return directory;
//	}


//
//
//                                                                                  Configuration
//                                                                           ---------------------------
//                                                          IsCustomizedBy   |                         |
//                                                        -----------------> | catalogue-configuration |
//                                                       /                   |                         |
//                                                      /                    ---------------------------
//     EService                        VirtualService  /
//   ------------               -----------------------------
//   |          |   CallsFor    |                           |
//   |   grsf   | ------------> | catalogue-virtual-service |
//   |          |               |                           |
//   ------------               -----------------------------
//                                                    \                                                                 EService
//                                                     \                                                          --------------------
//                                                      \                                             Uses        |                  |                         
//                                                       \                                    ------------------> | postgres-ckan-db |
//                                                        \                                  /                    |                  |
//                                                         \                     EService   /                     --------------------
//                                                          \                -----------------
//                                                           \   CallsFor    |               |
//                                                            -------------> |     ckan      |
//                                                                           |               |
//                                                                           -----------------                          EService   
//                                                                                          \                     --------------------
//                                                                                           \         Uses       |                  |
//                                                                                            ------------------> |       solr       |
//                                                                                                                |                  |
//                                                                                                                --------------------
//
	
	@Override
	protected ServiceCatalogueConfiguration readFromIS() {
		try {
			ServiceCatalogueConfiguration scc = super.readFromIS();
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(scc);
			GRSFCatalogueConfiguration grsfCC = objectMapper.readValue(json, GRSFCatalogueConfiguration.class);
			return grsfCC;
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
