<#include "macros.ftl">
<#assign identity_namespace="stock_identity" >
<#assign data_namespace="stock_data" >
{
	"name" : "${grsf_uuid?json_string}",
	<#if !is_patch && stock_name?has_content>
	"title" : "${stock_name?json_string}",
	</#if>
	<#if !is_patch && license_id?has_content>
	"license_id": "${license_id}",
	</#if>
	<#if !is_patch && description?has_content>
	"notes": "${description?json_string}",
	</#if>
	"version" : "${version?string["0.0"]}",
	"private": ${include_sensitive?c},
	<#if include_sensitive>
	"searchable": "True", <#-- If the Record is private we have to set searchable to True see https://support.d4science.org/issues/27922 -->
	</#if>	
	"extras": [
		<#-- START of Common Metadata -->
		<@metadata key="GRSF Type" namespace=identity_namespace var=grsf_type?capitalize />
		<@metadata key="Short Name" namespace=identity_namespace var=short_name />
		<@metadata key="GRSF Semantic Identifier" namespace=identity_namespace var=grsf_semantic_identifier />
		<#if database_sources??>
		<@metadatalist key="Database Source" namespace=identity_namespace list=database_sources />
			<#if source = "grsf">
				<#list database_sources as database_source>
					<#assign group_list += [{"name":"${database_source_name_to_id[database_source?json_string]}-group", "title":"${database_source?json_string}"}] >
				</#list>
			</#if>
		</#if>
		<@metadatalist key="Species" namespace=identity_namespace list=species tagValues=true />
		<@metadata key="Domain" var="Stock" />
		<@metadata key="GRSF UUID" var=grsf_uuid />
		<@metadata key="Citation" var=citation />
		<@metadatalistJson key="Annotation" list=annotations include=include_sensitive />
		<@metadata key="Record URL" var=record_url />
		<#-- END of Common Metadata -->
		
		<#-- Start of Stock/Fishery Common Metadata -->
		<@metadatalist key="Similar GRSF Record" namespace=identity_namespace list=similar_grsf_record />
		<@metadatalist key="Connected Fishery Record" namespace=identity_namespace list=connected />
		<@metadatalist key="Data Owner" namespace=data_namespace list=data_owner include=include_sensitive />
		<@timeseries key="Catch" namespace=data_namespace timeseries="catches" groupname="catch-group" />
		<@timeseries key="Landing" namespace=data_namespace timeseries="landings" groupname="landing-group" />

		<#if sdg_flag??>
		<@metadata key="SDG Flag" var=sdg_flag?c />
			<#if sdg_flag>
				<#assign group_list += [{"name":"grsf-sdg-flag-group", "title":"GRSF SDG Flag"}] >
			</#if>
		</#if>

		<@metadata key="Status of the Record" var=status_grsf_record />
		<@metadata key="spatial" var=spatial /> <#-- deprecated see https://support.d4science.org/issues/27843 -->
		<@metadata key="Map Preview" var=map_preview /> <#-- see https://support.d4science.org/issues/27843 -->
		<#-- END of Stock/Fishery Common Metadata -->

		<#-- START of Stock Metadata -->
		<@metadata key="GRSF Stock Name" namespace=identity_namespace var=stock_name />
		<@metadata key="Jurisdictional Distribution" namespace=identity_namespace var=jurisdictional_distribution />
		<@metadatalist key="Assessment Area" namespace=identity_namespace list=assessment_area tagValues=true />
		<@metadatalist key="Intersecting FAO Major Fishing Areas" namespace=identity_namespace list=hidden_assessment_area tagValues=true include=include_sensitive />
		<@metadata key="Assessor" namespace=data_namespace var=assessor include=include_sensitive />
		<@timeseries key="Assessment Method" namespace=data_namespace timeseries="assessment_methods" groupname="assessment-method-group" />
		<@timeseries key="Abundance Level (FIRMS Standard)" namespace=data_namespace timeseries="firms_standard_abundance_level" groupname="abundance-level-firms-standard-group" />
		<@timeseries key="Abundance Level" namespace=data_namespace timeseries="abundance_level" groupname="abundance-level-group" />
		<@timeseries key="Biomass" namespace=data_namespace timeseries="biomass" groupname="biomass-group" />
		<@timeseries key="Fishing Pressure (FIRMS Standard)" namespace=data_namespace timeseries="firms_standard_fishing_pressure" groupname="fishing-pressure-firms-standard-group" />
		<@timeseries key="Fishing Pressure" namespace=data_namespace timeseries="fishing_pressure" groupname="fishing-pressure-group" />
		<@timeseries key="State and Trend" namespace=data_namespace timeseries="state_and_trend_of_marine_resources" groupname="state-and-trend-group" />
		<@timeseries key="FAO Stock Status Category" namespace=data_namespace timeseries="fao_categories" groupname="fao-stock-status-category-group" />
		<@timeseries key="Scientific Advice" namespace=data_namespace timeseries="scientific_advice" groupname="scientific-advice-group" />
		<@timeseries key="Fishing Effort" namespace=data_namespace timeseries="fishing_effort" groupname="fishing-effort-group" />		
		<#-- END of Stock Metadata -->
		
		<#if source = "grsf">
		<@metadata key="system:type" var=grsf_type?capitalize sep="" />
		<#else>
		<@metadata key="system:type" var="Legacy" sep="" />
		</#if>
	],
	"organization": <@source_macro />
	"groups": [
		<#list group_list as group>
		{
			"name": "${group.name}",
			"title": "${group.title}"
		},
		</#list>
		<@group name="assessment-method-group" title="Assessment Method" var=assessment_methods />
		<@group name="scientific-advice-group" title="Scientific Advice" var=scientific_advice />
		<@group name="stock-group" title="Stock" var="OK" sep="" />
	],
	"tags": [
		<#list tag_list as tag>
			<@tagbyvalue value=tag include=true />
		</#list>
		<@tag tagname=connections_indicator />
		<@tag tagname=similarities_indicator />
		<@tag tagname=jurisdictional_distribution />
		<@tag tagname=grsf_type?capitalize sep="" />
	],
	"resources": [
		<@resources var=source_of_information /><#if refers_to?? && source_of_information??>,</#if>
		<@resources var=refers_to />
	],
	"timeseries": [
		<#list timeseries_list as elem>
			{
			<#list elem as key, value>
				"${key}":"${value}"<#sep>,</#sep>
			</#list>
			}<#sep>,</#sep>
		</#list>
	]
}