<#include "common.ftl">

<#assign timeseries_list = [] >
<#assign group_list = [] >
<#assign tag_list = [] >

<#if !is_patch>
	<#assign description = "Short Name: ${short_name}\n" >
	<#if grsf_semantic_identifier??>
		<#assign description += "GRSF Semantic Identifier: ${grsf_semantic_identifier}\n" >
	</#if>
	<#assign description += "Record URL: ${record_url}" >
</#if>

<#macro group name title var="" include=true sep=",">
	<#if var?has_content && include>
		{
			"name": "${name}",
			"title": "${title}"
		}${sep}
	</#if>
</#macro>

<#macro tag tagname="" include=true sep=",">
	<#if tagname?has_content && include>
		<@tagbyvalue value=tagname include=include sep=sep />
	</#if>
</#macro>

<#macro tags list include=true sep=",">
	<#if list?? && include>
		<#list list as tagname>
			<@tagbyvalue value=tagname include=include sep=sep />
		</#list>
	</#if>
</#macro>

<#macro tagbyvalue value include=true sep=",">
	<#assign tagvalue=value?json_string?replace("[,:]\\s{0,1}", " ", "r") />
		{
			<#if tagvalue?length gt 100 >
			"name": "${tagvalue[0..<99]}"
			<#else>
			"name": "${tagvalue}"
			</#if>
		}${sep}
</#macro>

<#macro metadata key namespace="" var="" include=true sep=",">
	<#if var?has_content && include>
		{
			"key": "<#if namespace?has_content>${namespace}:</#if>${key}",
			"value": "${var?json_string}"
		}${sep}
	</#if>
</#macro>

<#macro metadatalist key namespace="" list="" tagValues=false include=true sep=",">
	<#if list?has_content>
		<#list list as elem>
			<#if tagValues>
				<#assign tag_list += ["${elem?json_string}"]>
			</#if>
			<#if include>
			{
				"key": "<#if namespace?has_content>${namespace}:</#if>${key}",
				"value": "${elem?json_string}"
			}${sep}
			</#if>
		</#list>
	</#if>
</#macro>

<#macro metadatalistJson key namespace="" list="" include=true sep=",">
	<#if list?has_content && include>
		<#list list as elem>
		{
			"key": "<#if namespace?has_content>${namespace}:</#if>${key}",
			"value": "{<#list elem as key, value>\"${key}\":\"${value?json_string}\"<#sep>,</#sep></#list>}"
		}${sep}
		</#list>
	</#if>
</#macro>

<#macro timeseries key namespace="" timeseries="" groupname="" include=include_sensitive sep=",">
	<#if timeseries?has_content && .data_model[timeseries]??>
		<#assign timeseries_list += [{"property":"${timeseries}","filename":"${key}.csv"}]>
		<#if groupname?has_content>
			<#assign group_list += [{"name":"${groupname}", "title":"${key}"}] >
		</#if>
		<#if include>
			<#local list = .data_model[timeseries] >
			<#list list[0..*5] as ts>
				<#assign valueSep = "" >
				{
					"key": "<#if namespace?has_content>${namespace}:</#if>${key}",
					"value": "${ts.value?json_string} [<#if ts.unit?has_content>Unit: ${ts.unit?json_string}<#assign valueSep = " - " ></#if><#if ts.reporting_year_or_assessment_id?has_content>${valueSep}Rep. Year or Assessment ID: ${ts.reporting_year_or_assessment_id?json_string}<#assign valueSep = " - " ></#if><#if ts.reference_year?has_content>${valueSep}Ref. Year: ${ts.reference_year?replace(',','')}<#assign valueSep = " - " ></#if><#if ts.data_owner?has_content>${valueSep}Data Owner: ${ts.data_owner?json_string}<#assign valueSep = " - " ></#if><#if ts.db_source?has_content>${valueSep}DB Source: ${ts.db_source?json_string}</#if>]"
				}${sep}
			</#list>
		</#if>
	</#if>
</#macro>

<#macro resources var="" name="name" url="url" description="" include=true sep=",">
	<#if var?has_content>
		<#list var as v>
		{
			"name": "${v[name]}",
			<#if description?has_content>"description": "${v[description]?json_string}",</#if>
			"url": "${v[url]}"
		}<#sep>${sep}</#sep>
		</#list>
	</#if>
</#macro>

<#macro resource name="" url="" description="" include=true sep=",">
	<#if name?has_content && url?has_content>
		{
			"name": "${name}",
			<#if description?has_content>"description": "${description?json_string}",</#if>
			"url": "${url}"
		}${sep}
	</#if>
</#macro>

<#macro source_macro sep=",">
	<#local source_title = "${database_source_id_to_name[source]}" >
	<#if source = "grsf"> 
		<#assign group_list += [{"name":"${source}-group", "title":"${source_title}"}] >
	<#else>
		<#assign group_list += [{"name":"legacy-group", "title":"Legacy"}] >
	</#if>
	{
		"name": "${source}",
		"title": "${source_title}"
	}${sep}
</#macro>