<#include "macros.ftl">
<#assign identity_namespace="traceability_unit_identity" >
<#assign data_namespace="traceability_unit_data" >
{
	"name" : "${grsf_uuid?json_string}",
	<#if !is_patch && traceability_unit_name?has_content>
	"title" : "${traceability_unit_name?json_string}",
	</#if>
	<#if !is_patch && license_id?has_content>
	"license_id": "${license_id}",
	</#if>
	<#if !is_patch && description?has_content>
	"notes": "${description?json_string}",
	</#if>
	"version" : "${version?string["0.0"]}",
	"private": ${include_sensitive?c},
	<#if include_sensitive>
	"searchable": "True", <#-- If the Record is private we have to set searchable to True see https://support.d4science.org/issues/27922 -->
	</#if>	
	"extras": [
		<#-- START of Common Metadata -->
		<@metadata key="GRSF Type" namespace=identity_namespace var="Traceability Unit" />
		<@metadata key="Short Name" namespace=identity_namespace var=short_name />
		<@metadata key="GRSF Semantic Identifier" namespace=identity_namespace var=grsf_semantic_identifier />
		<#if database_sources??>
		<@metadatalist key="Database Source" namespace=identity_namespace list=database_sources />
			<#if source = "grsf">
				<#list database_sources as database_source>
					<#assign group_list += [{"name":"${database_source_name_to_id[database_source?json_string]}-group", "title":"${database_source?json_string}"}] >
				</#list>
			</#if>
		</#if>
		<@metadatalist key="Species" namespace=identity_namespace list=species tagValues=true />
		<@metadata key="Domain" var="Traceability Unit" />
		<@metadata key="GRSF UUID" var=grsf_uuid />
		<@metadata key="Citation" var=citation />
		<@metadatalistJson key="Annotation" list=annotations include=include_sensitive />
		<@metadata key="Record URL" var=record_url />
		<#-- END of Common Metadata -->
		
		<#-- START of Traceability Unit Metadata -->
		<@metadata key="GRSF Traceability Unit Name" namespace=identity_namespace var=traceability_unit_name />
		<@metadata key="Connected Stock Record" namespace=identity_namespace var=referring_stock_record.url />
		<@metadata key="Connected Fishery Record" namespace=identity_namespace var=referring_fishery_record.url />
		<@metadatalist key="Assessment Area" namespace=identity_namespace list=assessment_area />
		<@metadatalist key="Management/Reporting Area" namespace=identity_namespace list=management_reporting_area tagValues=true />
		<@metadatalist key="Flag State" namespace=identity_namespace list=flag_state tagValues=true />
		<@metadatalist key="Fishing Gear" namespace=identity_namespace list=fishing_gear tagValues=true />
		<@metadatalist key="Management Body/Authority" namespace=identity_namespace list=management_body_authorities />
		<@metadata key="Map Preview" var=map_preview /> <#-- see https://support.d4science.org/issues/27843 -->
		<#-- END of Traceability Unit Metadata -->
		
		<@metadata key="system:type" var="Traceability Unit" sep="" />
	],
	"organization": <@source_macro />
	"groups": [
		<#list group_list as group>
		{
			"name": "${group.name}",
			"title": "${group.title}"
		},
		</#list>
		<@group name="traceability-unit-group" title="Traceability Unit" var="OK" sep="" />
	],
	"tags": [
		<#list tag_list as tag>
			<@tagbyvalue value=tag include=true />
		</#list>
		<@tag tagname="Traceability Unit" sep="" />
	],
	"resources": [
		<#if !is_patch && referring_stock_record?has_content>
		<@resource name="GRSF Stock" url=referring_stock_record.url description=referring_stock_record.semantic_id />
		</#if>
		<#if !is_patch && referring_fishery_record?has_content>
		<@resource name="GRSF Fishery" url=referring_fishery_record.url description=referring_fishery_record.semantic_id />
		</#if>
		<@resource name="Traceability Unit URI" url=traceability_record_uri sep="" />
	],
	"timeseries": [
		<#list timeseries_list as elem>
			{
			<#list elem as key, value>
				"${key}":"${value}"<#sep>,</#sep>
			</#list>
			}<#sep>,</#sep>
		</#list>
	]
}