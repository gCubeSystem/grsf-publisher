<#assign keys = timeseries[0]?keys>
${keys?join(",")}
<#list timeseries as item>
<#list keys as key><#if item[key]?? && item[key]?has_content>${item[key]?c}<#else>""</#if><#sep>,</#sep></#list>
</#list>