<#assign database_source_id_to_name = { "grsf":"Global Record of Stocks and Fisheries (GRSF)", "ram":"RAM Legacy Stock Assessment Database", "firms":"Fisheries and Resources Monitoring System (FIRMS)", "fishsource":"FishSource", "sdg":"FAO SDG 14.4.1 Questionnaire"} >

<#assign database_source_name_to_id = {} >
<#list database_source_id_to_name as key, value>
	<#assign database_source_name_to_id += { "${value?json_string}" : "${key?json_string}" } >
</#list>

<#assign type_subtypes = { "Stock": ["Assessment Unit","Marine Resource","Legacy"], "Fishery": ["Fishing Unit","Other Fishery","Legacy"], "Traceability Unit": ["Traceability Unit"]} >
