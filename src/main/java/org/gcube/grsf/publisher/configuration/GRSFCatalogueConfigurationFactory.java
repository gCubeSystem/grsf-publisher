package org.gcube.grsf.publisher.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.gcat.configuration.isproxies.ISConfigurationProxy;
import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;
import org.gcube.gcat.utils.Constants;
import org.gcube.grsf.publisher.configuration.isproxies.impl.GRSFGCoreISConfigurationProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFCatalogueConfigurationFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(GRSFCatalogueConfigurationFactory.class);
	
	private static final Map<String, GRSFCatalogueConfiguration> grsfCatalogueConfigurations;
	
	private static List<ISConfigurationProxyFactory<?>> factories;
	
	static {
		grsfCatalogueConfigurations = new HashMap<>();
		factories = new ArrayList<>();
	}
	
	private static List<ISConfigurationProxyFactory<?>> getFactories(){
		if(factories.size()==0) {
//			factories.add(new GRSFFacetBasedISConfigurationProxyFactory());
			factories.add(new GRSFGCoreISConfigurationProxyFactory());
		}
		return factories;
	}
	
	public static void addISConfigurationProxyFactory(ISConfigurationProxyFactory<?> icpf) {
		factories.add(icpf);
	}
	
	private static GRSFCatalogueConfiguration load(String context) {
		GRSFCatalogueConfiguration grsfCatalogueConfiguration = null;
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
		
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				try {
					ISConfigurationProxy<?> icp = icpf.getInstance(context);
					
					grsfCatalogueConfiguration = (GRSFCatalogueConfiguration) icp.getCatalogueConfiguration();
					logger.trace("The configuration has been read using {}.", icp.getClass().getSimpleName());
				}catch(Exception e){
					logger.warn("{} cannot be used to read {}. Reason is {}", icpf.getClass().getSimpleName(), ServiceCatalogueConfiguration.class.getSimpleName(), e.getMessage());
				}
			}
			
		} catch(Exception e) {
			logger.error("Unable to start session. Reason is " + e.getMessage());
		} finally {
			secretManager.endSession();
		}
		
		if(grsfCatalogueConfiguration==null) {
			throw new RuntimeException("Unable to load " + ServiceCatalogueConfiguration.class.getSimpleName() + " by using configured " + ISConfigurationProxyFactory.class.getSimpleName() + " i.e. " + getFactories());
		}
		return grsfCatalogueConfiguration;
	}
	
	private static void purgeFromIS(String context) {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				ISConfigurationProxy<?> icp = icpf.getInstance(context);
				icp.delete();
			}
		} catch(Exception e) {
			logger.error("Unable to start session. Reason is " + e.getMessage());
		} finally {
			secretManager.endSession();
		}
	}
	
	private static void createOrUpdateOnIS(String context, ServiceCatalogueConfiguration catalogueConfiguration) throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		try {
			Secret secret = Constants.getCatalogueSecret();
			secretManager.startSession(secret);
			
			for(ISConfigurationProxyFactory<?> icpf : getFactories()) {
				ISConfigurationProxy<?> icp = icpf.getInstance(context);
				icp.setCatalogueConfiguration(catalogueConfiguration);
				icp.createOrUpdateOnIS();
			}
			
		} finally {
			secretManager.endSession();
		}
	}
	
	public synchronized static GRSFCatalogueConfiguration getInstance() {
		String context = SecretManagerProvider.instance.get().getContext();
		GRSFCatalogueConfiguration grsfCatalogueConfiguration = grsfCatalogueConfigurations.get(context);
		if(grsfCatalogueConfiguration == null) {
			grsfCatalogueConfiguration = load(context);
			grsfCatalogueConfigurations.put(context, grsfCatalogueConfiguration);
		}
		return grsfCatalogueConfiguration;
	}
	
	public synchronized static void renew() {
		String context = SecretManagerProvider.instance.get().getContext();
		grsfCatalogueConfigurations.remove(context);
		GRSFCatalogueConfiguration grsfCatalogueConfiguration = load(context);
		grsfCatalogueConfigurations.put(context, grsfCatalogueConfiguration);
	}
	
	public synchronized static void purge() {
		// Remove the resource from IS
		String context = SecretManagerProvider.instance.get().getContext();
		grsfCatalogueConfigurations.remove(context);
		purgeFromIS(context);
	}
	
	public synchronized static GRSFCatalogueConfiguration createOrUpdate(GRSFCatalogueConfiguration grsfCatalogueConfiguration) throws Exception {
		String context = SecretManagerProvider.instance.get().getContext();
		grsfCatalogueConfigurations.remove(context);
		
		createOrUpdateOnIS(context, grsfCatalogueConfiguration);
		grsfCatalogueConfigurations.put(context, grsfCatalogueConfiguration);
		
		// The supported organizations could be changed we need to empty the user cache for the context
		// to avoid to miss to add an user in an organization which has been added.
		CKANUserCache.emptyUserCache();
		
		return grsfCatalogueConfiguration;
	}
	
}
