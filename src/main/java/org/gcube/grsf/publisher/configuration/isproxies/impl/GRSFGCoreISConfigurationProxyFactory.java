package org.gcube.grsf.publisher.configuration.isproxies.impl;

import org.gcube.gcat.configuration.isproxies.ISConfigurationProxyFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFGCoreISConfigurationProxyFactory extends ISConfigurationProxyFactory<GRSFGCoreISConfigurationProxy> {
	
	public GRSFGCoreISConfigurationProxyFactory() {
		super();
	}
	
	@Override
	protected GRSFGCoreISConfigurationProxy newInstance(String context) {
		GRSFGCoreISConfigurationProxy isConfigurationProxy = new GRSFGCoreISConfigurationProxy(context);
		return isConfigurationProxy;
	}

	
	
	
}
