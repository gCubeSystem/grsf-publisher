package org.gcube.grsf.publisher.configuration;

import java.util.Map;
import java.util.Set;

import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.grsf.publisher.utils.OrganizationUtils;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFCatalogueConfiguration extends ServiceCatalogueConfiguration {

	protected boolean includeSensitive;

	public GRSFCatalogueConfiguration() {
		Map<String,String> organizationMap = OrganizationUtils.getInstance().getOrganizations();
		this.supportedOrganizations = organizationMap.keySet();
		this.defaultOrganization = "grsf";
		this.includeSensitive = true;
		this.socialPostEnabled = false;
		this.notificationToUsersEnabled = false;
	}
	
	@Override
	public boolean isModerationEnabled() {
		return false;
	}
	
	@Override
	public void setDefaultOrganization(String defaultOrganization) {
		// Nothing to do.
	}
	
	@Override
	public void setSupportedOrganizations(Set<String> supportedOrganizations) {
		// Nothing to do.
	}
		
	public boolean isIncludeSensitive() {
		return includeSensitive;
	}

	public void setIncludeSensitive(boolean includeSensitive) {
		this.includeSensitive = includeSensitive;
	}
	
}
