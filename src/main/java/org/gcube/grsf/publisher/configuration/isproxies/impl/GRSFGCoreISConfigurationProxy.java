package org.gcube.grsf.publisher.configuration.isproxies.impl;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.configuration.isproxies.impl.GCoreISConfigurationProxy;
import org.gcube.gcat.configuration.service.ServiceCatalogueConfiguration;
import org.gcube.grsf.publisher.configuration.GRSFCatalogueConfiguration;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFGCoreISConfigurationProxy extends GCoreISConfigurationProxy {

	public GRSFGCoreISConfigurationProxy(String context) {
		super(context);
	}
	
	@Override
	protected ServiceCatalogueConfiguration readFromIS() {
		try {
			ServiceCatalogueConfiguration scc = super.readFromIS();
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(scc);
			GRSFCatalogueConfiguration grsfCC = objectMapper.readValue(json, GRSFCatalogueConfiguration.class);
			return grsfCC;
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
