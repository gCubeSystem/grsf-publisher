package org.gcube.grsf.publisher.rest;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.gcube.gcat.api.GCatConstants;
import org.gcube.grsf.publisher.ckan.record.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@RequestHeaders ({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class BaseRESTAPIs<R extends Record> extends BaseREST {
	
	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Context
	protected HttpHeaders httpHeaders;
	
	@Context
	protected UriInfo uriInfo;
	
	protected static final String CONTENT_LOCATION_HEADER = "Content-Location";
	
	/* Used for accounting */
	protected final String COLLECTION_PARAMETER;
	/* Used for accounting */
	protected final String ID_PARAMETER;
	
	protected final Class<R> reference;
	
	public BaseRESTAPIs(String collectionName, String recordID, Class<R> reference) {
		this.COLLECTION_PARAMETER = collectionName;
		this.ID_PARAMETER = recordID;
		this.reference = reference;
	}

	protected R getInstance() {
		try {
			R record = reference.newInstance();
			record.setUriInfo(uriInfo);
			return record;
		} catch (WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	public String list(int limit, int offset) {
		Boolean countOnly = false;
		MultivaluedMap<String,String> queryParameters = uriInfo.getQueryParameters();
		if(queryParameters.containsKey(GCatConstants.COUNT_QUERY_PARAMETER)) {
			countOnly = Boolean.parseBoolean(queryParameters.get(GCatConstants.COUNT_QUERY_PARAMETER).get(0));
		}
		
		Record record = getInstance();
		
		if(queryParameters.containsKey(GCatConstants.Q_KEY)) {
			String q = queryParameters.get(GCatConstants.Q_KEY).get(0);
			q = String.format("%s AND extras_systemtype:%s", q, record.getType());
			queryParameters.add(GCatConstants.Q_KEY, q);
		}
		
		String ret = null;
		
		if(countOnly) {
			setCalledMethod("GET /" + COLLECTION_PARAMETER);
			int size = record.count();
			ret = createCountJson(size);
		}else {
			setCalledMethod("GET /" + COLLECTION_PARAMETER);
			return record.list(limit, offset);
		}
		
		String accept = httpHeaders.getHeaderString("Accept");
		if(accept!=null && accept.contains(GCatConstants.APPLICATION_JSON_API)) {
			return resultAsJsonAPI(ret);
		}
		return ret;
		
	}

	protected ResponseBuilder addContentLocation(ResponseBuilder responseBuilder, String url) {
		return responseBuilder.header(CONTENT_LOCATION_HEADER, url);
	}
	
	public Response create(String json) {
		setCalledMethod("POST /" + COLLECTION_PARAMETER);
		R record = getInstance();
		String ret = record.create(json);
		
		ResponseBuilder responseBuilder = Response.status(Status.CREATED).entity(ret);
		responseBuilder = addLocation(responseBuilder, record.getName());
		responseBuilder = addContentLocation(responseBuilder, record.getRecordURL());
		return responseBuilder.type(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8).build();
	}
	
	public String read(String id) {
		setCalledMethod("GET /" + COLLECTION_PARAMETER + "/{" + ID_PARAMETER + "}");
		Record record = getInstance();
		record.setName(id);
		return record.read();
	}
	
	public String update(String id, String json) {
		setCalledMethod("PUT /" + COLLECTION_PARAMETER + "/{" + ID_PARAMETER + "}");
		Record record = getInstance();
		record.setName(id);
		return record.update(json);
	}
	
	public String patch(String id, String json) {
		setCalledMethod("PATCH /" + COLLECTION_PARAMETER + "/{" + ID_PARAMETER + "}");
		Record record = getInstance();
		record.setName(id);
		return record.patch(json);
	}
	
	public Response delete(String id) {
		return delete(id, false);
	}
	
	public Response delete(String id, Boolean purge) {
		if(purge) {
			setCalledMethod("PURGE /" + COLLECTION_PARAMETER + "/{" + ID_PARAMETER + "}");
		} else {
			setCalledMethod("DELETE /" + COLLECTION_PARAMETER + "/{" + ID_PARAMETER + "}");
		}
		Record record = getInstance();
		record.setName(id);
		record.delete(purge);
		return Response.status(Status.NO_CONTENT).build();
	}
	
	public Response purge(String id) {
		return delete(id, true);
	}
	
	protected String resultAsJsonAPI(String data) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("{ \"data\":");
		stringBuffer.append(data);
		stringBuffer.append("}");
		return stringBuffer.toString();
	}

}
