package org.gcube.grsf.publisher.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.gcube.gcat.api.GCatConstants;
import org.gcube.grsf.publisher.annotation.PATCH;
import org.gcube.grsf.publisher.annotation.PURGE;
import org.gcube.grsf.publisher.ckan.record.FisheryRecord;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(FisheryRESTAPIs.COLLECTION_PATH)
@ResourceGroup("Fishery APIs")
@ResourceLabel("Fishery APIs")
public class FisheryRESTAPIs extends BaseRESTAPIs<FisheryRecord> {
	
	public static final String COLLECTION_PATH = "fishery";
	public static final String GRSF_RECORD_UUID_PARAMETER = "FISHERY_GRSF_RECORD_UUID";
	
	public FisheryRESTAPIs() {
		super(COLLECTION_PATH, GRSF_RECORD_UUID_PARAMETER, FisheryRecord.class);
	}
	
	/**
	 * <p>
	 * 	The listing API provides paginated results by using the query parameters limit and offset.<br/>
	 * 	It returns an array list of string containing the GRSF UUIDs of the fishery records.<br/>
	 * 	Each name can be used as <code>{fishery_record_id}</code> path parameter to manage such a record.
	 * </p>
	 * 
	 * <h3>Filtering options</h3>
	 * <p>
	 * 	The listing method offers options to filter the results, thus enacting to search for records including spatial search (see ext_bbox below).<br/>
	 * </p>
	 * 
	 * <h4>Basic Filtering options</h4>
	 * <dl>
	 * 	<dt>include_private (bool)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:false</em>.<br/>
	 * 		If True, private records will be included in the results.<br/>
	 * 		For the sysadmins will be returned all private datasets.<br/>
	 * 		E.g. <code>/fishery?include_private=true</code>
	 * 	</dd>
	 * 	
	 * 	<dt style="margin-top: 5px;">ext_bbox</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:null</em>.<br/>
	 * 		The coordinates of the upper-right and bottom-left angle of a rectangular to query for.
	 * 		The form is <code>Lat,Long,Lat,Long</code><br/>
	 * 		E.g. <code>/fishery?limit=10&offset=0&q=Gadus&ext_bbox=-7.535093,49.208494,3.890688,57.372349</code> 
	 * 		returns the first 10 records with 'Gadus' having a spatial coverage in the specified bounding box.
	 * 	</dd>
	 * 
	 * </dl>
	 * 
	 * <h4>Filtering options based on Solr query parameters</h4>
	 * <p>
	 * 	It accepts the following query parameters (a subset of Solr search query parameters, see {@see <a href="https://solrtutorial.com/solr-query-syntax.html">Solr Query Syntax</a>}):
	 * </p>
	 * <dl>
	 * 	<dt>q (string)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:"*:*"</em><br/>
	 * 		The solr query.<br/>
	 *  	E.g. <code>/fishery?q=title:Gadus</code> returns the fishery records with word "Gadus" in the title.<br/>
	 *  	E.g. <code>/fishery?q=extras_systemtype:Fishing Unit</code> returns the GRSF <em>Fishing Unit</em> records.
	 *  </dd>
	 * 
	 *  <dt style="margin-top: 5px;">fq (string)</dt>
	 *  <dd>
	 *  	<em>Optional</em>.<em>Default:null</em>.<br/>
	 *  	Filter query. A query string that limits the query results without influencing their scores.<br/>
	 *  	E.g. <code>/fishery?q=title:Gadus&fq=annotations:test</code> returns with word "Gadus" in the 'title' and the word "test" in the 'Annotations'.
	 * 	</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">fq_list (list of strings)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:null</em>.<br/>
	 * 		Additional filter queries to apply.<br/>
	 * 		E.g. <code>/fishery?q=title:Gadus&fq_list=...</code> returns the records with word "Gadus" in the 'title'.
	 * 	</dd> 
	 * 
	 * 	<dt style="margin-top: 5px;">sort (string)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:"relevance asc, metadata_modified desc"</em>.<br/>
	 * 		Sorting of the search results.<br/> 
	 * 		As per the solr documentation, this is a comma-separated string of field names and sort-orderings.<br/>
	 * 		E.g. <code>/fishery?q=title:Gadus&sort=name+asc</code> returns the records with word "Gadus" in the 'title' 
	 * 		sorting the results by name ascending.
	 * 	</dd>
	 * <dl>
	 * 
	 * <h3>Query results options</h3>
	 * <p>
	 * 	The result is by default an array list of string containing the GRSF UUIDs of the records.
	 * 	Anyway, there are two options to get a different results.
	 * </p>
	 * <dl>
	 * 	<dt>count (bool)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:false</em>.<br/>
	 * 		If True, it indicates that the result must contains only the total number of records of the query.<br/>
	 * 		E.g. <code>/fishery?limit=10&offset=0&count=true</code>
	 * 	</dd>
	 * 	
	 * 	<dt style="margin-top: 5px;">all_fields (bool)</dt>
	 * 	<dd>
	 * 		<em>Optional</em>.<em>Default:false</em>.
	 * 		If True, the returned array list contains the whole records representation and not only the GRSF UUIDs of the records.</br/>
	 * 		E.g. <code>/fishery?limit=10&offset=0&all_fields=true</code>
	 * 	</dd>
	 * </dl>
	 * <p>
	 * 	Please note that, <code>count</code> query parameter has priority over <code>all_fields</code> query parameter.
	 * 	In other words,  <code>all_fields</code> query parameter is not considered is <code>count</code> query parameter is true.
	 * </p> 
	 * 
	 * 
	 * @param limit (<em>Default:10</em>) To get unlimited results the limit query parameters must be set to -1. 
	 * 	If the results are too much the operation could fail. 
	 * 	It is recommended to request no more than 1000 results. 
	 * @param offset (<em>Default:0</em>) The offset parameter indicates the starting position of the result.
	 * @return It returns an array list of string containing the GRSF UUIDs of the records.
	 * 	E.g.<pre>["GRSF_UUID_0","GRSF_UUID_1",...,"GRSF_UUID_10"]</pre>
	 * 	
	 * 	In the case the query parameter <code>count=true</code> it returns the total number of records of the query.
	 *  E.g. <pre>{"count":148}</pre>
	 *  
	 *  In the case the query parameter <code>all_fields=true</code> each element of the resulting array contains the record representation: 
	 *  E.g.
	 *  <pre>
	 *  [
	 *  	{
	 *  		"name"="GRSF_UUID_0",
	 *  		...,
	 *  		"private": false,
	 *  		"license_id": "CC-BY-SA-4.0",
	 *  	},
	 *  	{
	 *  		"name"="GRSF_UUID_1",
	 *  		...,
	 *  		"private": false,
	 *  		"license_id": "CC-BY-SA-4.0",
	 *  	},
	 *  	...,
	 *  	{
	 *  		"name"="GRSF_UUID_N",
	 *  		...,
	 *  		"private": false,
	 *  		"license_id": "CC-BY-SA-4.0",
	 *  	}
	 *  
	 *  ]</pre>
	 *  
	 *  @pathExample /fishery?limit=10&offset=0
	 *  @responseExample application/json;charset=UTF-8 ["GRSF_UUID_0","GRSF_UUID_1",...,"GRSF_UUID_N"]
	 */
	@GET
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 200, condition = "The request succeeded.")
	})
	public String list(@QueryParam(GCatConstants.LIMIT_QUERY_PARAMETER) @DefaultValue("10") int limit,
			@QueryParam(GCatConstants.OFFSET_QUERY_PARAMETER) @DefaultValue("0") int offset) {
		return super.list(limit, offset);
	}
	
	/**
	 * This API allows to create a fishery record
	 * @pathExample /fishery
	 * 
	 * @requestExample application/json;charset=UTF-8 classpath:/api-docs-examples/fishery/create-fishery-request.json
	 * @responseExample application/json;charset=UTF-8 classpath:/api-docs-examples/fishery/create-fishery-response.json
	 */
	@POST
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 201, condition = "The fishery record has been created successfully.")
	})
	public Response create(String json) {
		return super.create(json);
	}
	
	/**
	 * This API allows to read a fishery record
	 * @param id the GRSF UUID of the fishery record to read
	 * @pathExample /fishery/d0145931-58d3-4561-bf64-363b61df016b
	 * @responseExample application/json;charset=UTF-8 classpath:/api-docs-examples/fishery/read-fishery-response.json
	 */
	@GET
	@Path("/{" + GRSF_RECORD_UUID_PARAMETER + "}")
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 200, condition = "The fishery record exists.")
	})
	@Override
	public String read(@PathParam(GRSF_RECORD_UUID_PARAMETER) String id) {
		return super.read(id);
	}
	
	/**
	 * This API allows to update a fishery record
	 * @param id the GRSF UUID of the fishery record to update
	 * @pathExample /fishery/d0145931-58d3-4561-bf64-363b61df016b
	 * @requestExample application/json;charset=UTF-8 classpath:/api-docs-examples/fishery/create-fishery-request.json
	 * @responseExample application/json;charset=UTF-8 classpath:/api-docs-examples/fishery/create-fishery-response.json
	 */
	@PUT
	@Path("/{" + GRSF_RECORD_UUID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 200, condition = "The fishery record has been updated successfully.")
	})
	@Override
	public String update(@PathParam(GRSF_RECORD_UUID_PARAMETER) String id, String json) {
		return super.update(id, json);
	}
	
	/**
	 * This API allows to patch a fishery record.
	 * The service only updates the provided properties and, 
	 * eventually, creates resources (e.g., time series) and 
	 * adds tags or groups to the records.
	 * The API does not guarantee that the record is removed from an 
	 * old group/tag associated due to the old property value.
	 * @param id the GRSF UUID of the fishery record to patch
	 * @pathExample /fishery/d0145931-58d3-4561-bf64-363b61df016b
	 */
	@PATCH
	@Path("/{" + GRSF_RECORD_UUID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@StatusCodes ({
		@ResponseCode ( code = 200, condition = "The fishery record has been patched successfully.")
	})
	@Override
	public String patch(@PathParam(GRSF_RECORD_UUID_PARAMETER) String id, String json) {
		return super.patch(id, json);
	}
	
	/**
	 * This API allows to delete/purge (when the purge query parameter is true) 
	 * a fishery record.
	 * In case of delete, the fishery record is no longer visible, 
	 * but it still exists and can be restored.
	 * In case of purge, the record is completely deleted and cannot be restored.
	 * @param id the GRSF UUID of the fishery record to delete/purge
	 * @param purge (default false) a boolean indicating if the fishery record must be deleted or purged (when true)
	 * @pathExample /fishery/d0145931-58d3-4561-bf64-363b61df016b?purge=true
	 */
	@DELETE
	@Path("/{" + GRSF_RECORD_UUID_PARAMETER + "}")
	@StatusCodes ({
		@ResponseCode ( code = 204, condition = "The fishery record has been deleted successfully."),
		@ResponseCode ( code = 404, condition = "The fishery record was not found.")
	})
	@Override
	public Response delete(@PathParam(GRSF_RECORD_UUID_PARAMETER) String id,
			@QueryParam(GCatConstants.PURGE_QUERY_PARAMETER) @DefaultValue("false") Boolean purge) {
		return super.delete(id, purge);
	}
	
	/**
	 * This API allows to purge a fishery record.
	 * The record cannot be restored.
	 * @param id the GRSF UUID of the fishery record to purge
	 * @pathExample /fishery/d0145931-58d3-4561-bf64-363b61df016b
	 */
	@PURGE
	@Path("/{" + GRSF_RECORD_UUID_PARAMETER + "}")
	@StatusCodes ({
		@ResponseCode ( code = 204, condition = "The fishery record has been purged successfully."),
		@ResponseCode ( code = 404, condition = "The fishery record was not found.")
	})
	@Override
	public Response purge(@PathParam(GRSF_RECORD_UUID_PARAMETER) String id) {
		return super.purge(id);
	}
	
}
