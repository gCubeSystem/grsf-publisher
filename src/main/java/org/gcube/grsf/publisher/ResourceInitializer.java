package org.gcube.grsf.publisher;

import javax.ws.rs.ApplicationPath;

import org.gcube.grsf.publisher.rest.BaseRESTAPIs;
import org.gcube.grsf.publisher.rest.administration.Configuration;
import org.gcube.smartgears.annotations.ManagedBy;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(GRSFInitializator.class)
public class ResourceInitializer extends ResourceConfig {
	
	public ResourceInitializer() {
		packages(BaseRESTAPIs.class.getPackage().toString());
		packages(Configuration.class.getPackage().toString());
	}
	
}
