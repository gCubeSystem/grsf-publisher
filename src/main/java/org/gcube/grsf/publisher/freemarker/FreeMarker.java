package org.gcube.grsf.publisher.freemarker;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FreeMarker {

	protected Configuration configuration;
	protected File freemarkerDirectory;
	
//	protected File getResourcesDirectory() throws Exception {
//		URL fileURL = FreeMarker.class.getClassLoader().getResource("config.properties");
//		File logbackFile = new File(fileURL.toURI());
//		File resourcesDirectory = logbackFile.getParentFile();
//		return resourcesDirectory;
//	}
	
	protected File getFreemarkerDirectory() throws Exception {
		if(freemarkerDirectory==null) {
			URL directoryURL = FreeMarker.class.getClassLoader().getResource("freemarker");
			freemarkerDirectory = new File(directoryURL.toURI());
		}
		return freemarkerDirectory;
	}
	
	protected Configuration getConfiguration() throws Exception {
		if(configuration ==null) {
			configuration = new Configuration(Configuration.VERSION_2_3_23);
			configuration.setDirectoryForTemplateLoading(getFreemarkerDirectory());
			configuration.setDefaultEncoding("UTF-8");
			configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		}
		return configuration;
	}
	
	public Template getTemplate(String templateName) throws Exception {
		Configuration configuration = getConfiguration();
		Template template = configuration.getTemplate(templateName);
		return template;
	}
	
	public Map<String, Object> getMap(String json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
	    @SuppressWarnings("unchecked")
		Map<String, Object> map = mapper.readValue(json, Map.class);
	    return map;
	}
	
	public String generateJson(String templateName, String dataAsJsonString) throws Exception {
		Template template = getTemplate(templateName);
		Map<String, Object> map = getMap(dataAsJsonString);
		StringWriter writer = new StringWriter();
		generateJson(template, map, writer);
		return writer.toString();
	}
	
	public void generateJson(Template template, Map<String, Object> map, Writer writer) throws Exception { 
		template.process(map, writer);
        writer.flush();
        writer.close();
	}
	
}
