package org.gcube.grsf.publisher.utils;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.core.type.TypeReference;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.grsf.publisher.freemarker.FreeMarker;

import freemarker.template.Template;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class OrganizationUtils {

	private JsonNode organizationJsonNode;
	private Map<String, String> organizations;
	
	public static final OrganizationUtils getInstance() {
		return new OrganizationUtils();
	}
	
	protected OrganizationUtils() {
		this.organizationJsonNode = null;
		this.organizations = null;
	}
	
	protected void init() {
		FreeMarker freeMarker = new FreeMarker();
		StringWriter writer = new StringWriter();
		try {
			Template orgTemplate = freeMarker.getTemplate("organizations.ftl");
			
		    orgTemplate.process(new HashMap<>(), writer);
		    writer.flush();
		    writer.close();
		    
		    ObjectMapper mapper = new ObjectMapper();
		    organizationJsonNode = mapper.readTree(writer.toString());
		    organizations = mapper.convertValue(organizationJsonNode, new TypeReference<Map<String, String>>(){});
		    
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Map<String, String> getOrganizations() {
		if(organizations==null || organizationJsonNode==null) {
			init();
		}
		return organizations;
	}

	public JsonNode getOrganizationJsonNode() {
		if(organizations==null || organizationJsonNode==null) {
			init();
		}
		return organizationJsonNode;
	}

}
