package org.gcube.grsf.publisher.utils;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.core.type.TypeReference;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.grsf.publisher.freemarker.FreeMarker;

import freemarker.template.Template;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeUtils {

	private JsonNode typesJsonNode;
	private Map<String, Set<String>> types;
	
	public static final TypeUtils getInstance() {
		return new TypeUtils();
	}
	
	protected TypeUtils() {
		this.typesJsonNode = null;
		this.types = null;
	}
	
	protected void init() {
		FreeMarker freeMarker = new FreeMarker();
		StringWriter writer = new StringWriter();
		try {
			Template orgTemplate = freeMarker.getTemplate("types.ftl");
			
		    orgTemplate.process(new HashMap<>(), writer);
		    writer.flush();
		    writer.close();
		    
		    ObjectMapper mapper = new ObjectMapper();
		    typesJsonNode = mapper.readTree(writer.toString());
		    types = mapper.convertValue(typesJsonNode, new TypeReference<Map<String,Set<String>>>(){});
		    
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public Map<String,Set<String>> getTypes() {
		if(types==null || typesJsonNode==null) {
			init();
		}
		return types;
	}

	public JsonNode getTypesJsonNode() {
		if(types==null || typesJsonNode==null) {
			init();
		}
		return typesJsonNode;
	}

}
