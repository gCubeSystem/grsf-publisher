package org.gcube.grsf.publisher.ckan.record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class StockRecord extends Record {

	@Override
	public String getType() {
		return "Stock";
	}
	
}
