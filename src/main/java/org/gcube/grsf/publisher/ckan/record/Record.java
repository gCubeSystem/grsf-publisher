package org.gcube.grsf.publisher.ckan.record;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.UriInfo;

import org.gcube.com.fasterxml.jackson.core.type.TypeReference;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.com.fasterxml.jackson.databind.node.TextNode;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKAN;
import org.gcube.gcat.persistence.ckan.CKANPackage;
import org.gcube.gcat.persistence.ckan.CKANResource;
import org.gcube.gcat.persistence.ckan.CKANUtility;
import org.gcube.gcat.utils.Constants;
import org.gcube.gcat.utils.URIResolver;
import org.gcube.grsf.publisher.ckan.others.GRSFResource;
import org.gcube.grsf.publisher.configuration.GRSFCatalogueConfiguration;
import org.gcube.grsf.publisher.configuration.GRSFCatalogueConfigurationFactory;
import org.gcube.grsf.publisher.freemarker.FreeMarker;
import org.gcube.grsf.publisher.utils.TypeUtils;
import org.gcube.grsf.publisher.workspace.GRSFStorageHubManagement;
import org.gcube.storagehub.StorageHubManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Template;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class Record extends CKANPackage {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String RECORD_URL_TEMPLATE_PROPERTY_KEY = "record_url";
	public static final String INCLUDE_SENSITIVE_TEMPLATE_PROPERTY_KEY = "include_sensitive";
	public static final String IS_PATCH_TEMPLATE_PROPERTY_KEY = "is_patch";
	
	public static final String GRSF_UUID_PROPERTY = "grsf_uuid";
	public static final String TAGS_PROPERTY = "tags";
	public static final String GROUPS_PROPERTY = "groups";
	public static final String ORGANIZATION_PROPERTY = "organization";
	public static final String NAME_PROPERTY = "name";
	
	public static final String RESOURCES_PROPERTY = "resources";
	public static final String RESOURCE_ELEMENT_URL_PROPERTY = "url";
	public static final String TIMESERIES_PROPERTY = "timeseries";
	public static final String TIMESERIES_ELEMENT_PROPERTY_PROPERTY = "property";
	public static final String TIMESERIES_ELEMENT_FILENAME_PROPERTY = "filename";
	
	public static final String TIMESERIES_RESOURCE_FILE_EXTENSION = ".csv";
	public static final String TIMESERIES_MIMETYPE = "text/csv";
	public static final String TIMESERIES_FORMAT = "CSV";
	
	public static final String TYPE_QUERY_FILTER_KEY = "extras_systemtype";
		
	public static final Pattern TAG_PATTERN = Pattern.compile("[^\\s\\w-_.]");
    
	protected UriInfo uriInfo;
	
	protected String grsfUUID;
	
	protected ObjectMapper objectMapper; 
	protected JsonNode jsonNode; 
	
	protected boolean patch;
	
	protected Set<FileContainer> wsUploadedFiles;
	
	public Record() {
		super(GRSFCatalogueConfigurationFactory.getInstance());
		this.objectMapper = new ObjectMapper();
		this.patch = false;
	}
	
	public abstract String getType();
	
	@Override
	protected void parseResult() {
		super.parseResult();
		this.name = result.get(NAME_KEY).asText();		
	}
	
	public JsonNode elaborate(JsonNode node) throws Exception {
		FreeMarker freeMarker = new FreeMarker();
		Template tsTemplate = freeMarker.getTemplate("timeseries.ftl");
		Template template = freeMarker.getTemplate(getType() + ".ftl");
		Map<String, Object> map = getMapFromSourceJson(node);
		File transformedJson = null;
		try {
			transformedJson = getTransformedJsonFile(template,map);
		}catch (Exception e) {
			throw new BadRequestException("Error while applying JSON transformation", e);
		}
		
		try {
			jsonNode = mapper.readTree(transformedJson);
			jsonNode = elaborateTimeSeries(tsTemplate,map);
			jsonNode = checkTags(jsonNode);
			jsonNode = checkOrganization(jsonNode);
			jsonNode = checkGroups(jsonNode);
		} finally {
			transformedJson.delete();
		}
		
		return jsonNode;	
	}

	protected JsonNode checkOrganization(JsonNode jsonNode) {
		ObjectNode org = (ObjectNode) jsonNode.get(Record.ORGANIZATION_PROPERTY);
		String orgId = org.get(Record.NAME_PROPERTY).asText();
		((ObjectNode) jsonNode).put(CKAN.OWNER_ORG_KEY, orgId);
		((ObjectNode) jsonNode).remove(Record.ORGANIZATION_PROPERTY);
		return jsonNode;
	}

	protected JsonNode checkTags(JsonNode jsonNode) {
		if(jsonNode.has(Record.TAGS_PROPERTY)) {
			ArrayNode tags = (ArrayNode) jsonNode.get(Record.TAGS_PROPERTY);
			for(JsonNode tag : tags) {
				String t = tag.get(Record.NAME_PROPERTY).asText();
				Matcher tagMatcher = Record.TAG_PATTERN.matcher(t);
				String newT = tagMatcher.replaceAll("");
				newT = newT.replaceAll("  ", " ");
				((ObjectNode)tag).replace(Record.NAME_PROPERTY, new TextNode(newT));
			}
		}
		return jsonNode;
	}
	
	protected JsonNode checkGroups(JsonNode jsonNode) {
		// TODO check groups existence and user to group association 
		
		/*
		if(jsonNode.has(Record.TAGS_PROPERTY)) {
			ArrayNode groups = (ArrayNode) jsonNode.get(Record.TAGS_PROPERTY);
			for(JsonNode group : groups) {
				ckanUser.addToGroup(group.get(NAME_PROPERTY).asText());
			}
		}
		*/
		
		return jsonNode;
	}
	
	protected StringWriter addTypesFilters(StringWriter stringWriter) {
		Map<String,Set<String>> types = TypeUtils.getInstance().getTypes();
		Set<String> subtypes = types.get(getType());
		
		int i=1;
		stringWriter.append(" (");
		for(String subtype : subtypes) {
			stringWriter.append(TYPE_QUERY_FILTER_KEY);
			stringWriter.append(":");
			stringWriter.append("\"");
			stringWriter.append(subtype);
			stringWriter.append("\"");
			
			if(i!=subtypes.size()) {
				// Please note that an item can only belong to a single organization. 
				// Hence the query must put supported organizations in OR.
				stringWriter.append(" OR ");
			}
			
			i++;
		}
		stringWriter.append(") ");
		
		return stringWriter;
	}
	
	protected StringWriter addGroupFilter(StringWriter stringWriter) {
		stringWriter.append(" (");
		stringWriter.append("groups:");
		stringWriter.append(getType().toLowerCase().replaceAll(" ", "-"));
		stringWriter.append("-group");
		stringWriter.append(") ");
		return stringWriter;
	}
	
	@Override
	protected Map<String,String> getListingParameters(int limit, int offset, String... requiredFields){
		Map<String,String> parameters = super.getListingParameters(limit, offset);
		String q = parameters.get(GCatConstants.Q_KEY);
		StringWriter stringWriter = new StringWriter();
		
		if(q.contains(TYPE_QUERY_FILTER_KEY)) {
			throw new BadRequestException("To filter between types you need to use the dedicated collection");
		}
		
		if(q!=null) {
			stringWriter.append(q);
			if(q.length()>0) {
				stringWriter.append(" AND ");
			}
		}
		
		addTypesFilters(stringWriter);

		/* 
		 * Legacy type contains both Stock and Fishery
		 * hence we need group filtering too.
		 * Even group filtering should be sufficient 
		 * I preferred to add also type filtering too.
		 */
		stringWriter.append(" AND ");
		addGroupFilter(stringWriter);
		
		parameters.put(GCatConstants.Q_KEY, stringWriter.toString());
		return parameters;
	}
	
	
	/**
	 * Save the file in the workspace using storagehub-application-persistence
	 * @param file the file to persist in the workspace
	 * @return 
	 * @throws Exception 
	 */
	protected FileContainer persistFile(File file) throws Exception{
		StorageHubManagement shm = new StorageHubManagement();
		FileInputStream fis = new FileInputStream(file);
		shm.persistFile(fis, file.getName(), TIMESERIES_MIMETYPE);
		FileContainer fileContainer = shm.getPersistedFile();
		
		logger.debug("File {} has been persisted in StorageHub with ID:{}", file.getName(), fileContainer.getId());
		return fileContainer;
	}
	
	@Override
	public String getRecordURL() {
		if(recordURL==null) {
			recordURL = uriResolver.getCatalogueItemURL(grsfUUID);
		}
		return recordURL;
	}
	
	protected Map<String, Object> getMapFromSourceJson(JsonNode jsonNode) throws Exception {
		Map<String, Object> map = objectMapper.convertValue(jsonNode, new TypeReference<Map<String, Object>>(){});
		grsfUUID = map.get(Record.GRSF_UUID_PROPERTY).toString();
		
		map.put(RECORD_URL_TEMPLATE_PROPERTY_KEY, getRecordURL());
		
		map.put(INCLUDE_SENSITIVE_TEMPLATE_PROPERTY_KEY, ((GRSFCatalogueConfiguration) configuration).isIncludeSensitive());
		
		map.put(IS_PATCH_TEMPLATE_PROPERTY_KEY, patch);
		
		return map;
	}
	
	protected File getTransformedJsonFile(Template template, Map<String, Object> map) throws Exception {
		File out = new File(grsfUUID+".json");
	    out.delete();
		
		Writer writer = new FileWriter(out);
	    template.process(map, writer);
	    writer.flush();
	    writer.close();
	    
	    return out;
	}
	
	public static String getFilename(String grsfUUID, String timeSeriesFileName) {
		return grsfUUID + "-" + timeSeriesFileName;
	}
	
	protected JsonNode elaborateTimeSeries(Template tsTemplate, Map<String, Object> map) throws Exception {
		ArrayNode resources = (ArrayNode) jsonNode.get(Record.RESOURCES_PROPERTY);
		this.wsUploadedFiles = new HashSet<>();
		
		ArrayNode timeseries = (ArrayNode) jsonNode.get(Record.TIMESERIES_PROPERTY);
		List<File> timeseriesFiles = new ArrayList<>();
		
		Secret secret = Constants.getCatalogueSecret();
		SecretManager secretManager = SecretManagerProvider.instance.get();
		
		try {
			secretManager.startSession(secret);
			for(JsonNode n : timeseries) {
				String key = n.get(Record.TIMESERIES_ELEMENT_PROPERTY_PROPERTY).asText();
				try {
			    	String fileName = n.get(Record.TIMESERIES_ELEMENT_FILENAME_PROPERTY).asText();
			        
			    	logger.trace("Elaborating {} timeseries from property {} of Record with GRSF UUID {}", getType(), key, grsfUUID);
			        
			    	/*
			    	 * Adding to map the name of the property containing the timeseries
			    	 * It is used by the FreeMarker template to get json property to be used 
			    	 * to generate the timeseries  
			    	 */
		        	map.put(Record.TIMESERIES_PROPERTY, map.get(key));
			        
		        	String finalFilename = getFilename(grsfUUID, fileName); 
		        	
		        	File tsOut = new File(finalFilename);
		        	tsOut.delete();
					
		        	Writer tsWriter = new FileWriter(tsOut);
		        	tsTemplate.process(map, tsWriter);
		        	tsWriter.flush();
		        	tsWriter.close();
		        	
		        	timeseriesFiles.add(tsOut);
		        	
		        	/*
		        	 * Removing the property from the map to keep it   
		        	 * as generated using the input json
		        	 */
		        	map.remove(Record.TIMESERIES_PROPERTY);
		        	
		        	
		        	GRSFStorageHubManagement grsfSHM = new GRSFStorageHubManagement();
		        	grsfSHM.setGrsfUUID(grsfUUID);
		        	grsfSHM.persistFile(tsOut);
		        	this.wsUploadedFiles.add(grsfSHM.getFileContainer());
		        	URL url = grsfSHM.getPublicLink();
		        	
		        	
		        	ObjectNode resourceNode = objectMapper.createObjectNode();
		        	resourceNode.put(NAME_PROPERTY, fileName.replace(TIMESERIES_RESOURCE_FILE_EXTENSION, ""));
		        	resourceNode.put(RESOURCE_ELEMENT_URL_PROPERTY, url.toString());
		        	resourceNode.put(CKANResource.MIME_TYPE_KEY, TIMESERIES_MIMETYPE);
		        	resourceNode.put(CKANResource.FORMAT_KEY, TIMESERIES_FORMAT);
		        	resources.add(resourceNode);
		        	((ObjectNode)jsonNode).replace(RESOURCES_PROPERTY, resources);
		        	
		        	timeseriesFiles.remove(tsOut);
		        	tsOut.delete();
				}catch (Exception e) {
					logger.error("Something went wrong generating {} timeseries from property {} of Record with GRSF UUID {}\n{}", getType(), key, grsfUUID, e);
					throw e;
				}
		    }
			
			((ObjectNode)jsonNode).remove(Record.TIMESERIES_PROPERTY);
		} finally {
			secretManager.endSession();
			/*
			 * Remove the local files of timeseries
			 * if an error occurs to keep the environment clean 
			 */
	    	for(File f : timeseriesFiles) {
	    		f.delete();
	    	}
		}
		
	    return jsonNode;
	}
	
	public String simulateCreation(String json) {
		try {
			JsonNode jNode = objectMapper.readTree(json);
			elaborate(jNode);
			
			jsonNode = checkAndFixAuthorAndMaintainer((ObjectNode) jsonNode);
			
			String jsonString = objectMapper.writeValueAsString(jsonNode);
			
			return jsonString;
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		} finally {
			deleteUploadedFiles();
		}
	}

	@Override
	public String create(String json) {
		try {
			JsonNode jNode = objectMapper.readTree(json);
			elaborate(jNode);
			
			jsonNode = checkAndFixAuthorAndMaintainer((ObjectNode) jsonNode);
			
			String jsonString = objectMapper.writeValueAsString(jsonNode);
			sendPostRequest(CREATE, jsonString);
			
			parseResult();
			
			return getAsCleanedString(result);
			
		} catch(WebApplicationException e) {
			deleteUploadedFiles();
			throw e;
		} catch(Exception e) {
			deleteUploadedFiles();
			throw new InternalServerErrorException(e);
		}
	}

	private void deleteUploadedFiles() {
		
		try {
			if(wsUploadedFiles==null || wsUploadedFiles.size()==0) {
				return;
			}
			Secret secret = Constants.getCatalogueSecret();
			SecretManager secretManager = SecretManagerProvider.instance.get();
			secretManager.startSession(secret);
			try {
				for(FileContainer fileContainer : wsUploadedFiles) {
					try {
						logger.debug("Going to delete from StorageHub the file with ID:{}", fileContainer.getId());
						fileContainer.delete();
						logger.debug("StorageHub file with ID:{} has been properly removed", fileContainer.getId());
					} catch (StorageHubException e) {
							logger.warn("Unable to delete from StorageHub the file with ID:{}", fileContainer.getId());
					}
				}
			} finally {
				secretManager.endSession();
			}
		}catch (Exception e) {
			logger.warn("Error while removing uploaded file in StorageHub", e);
		}
	}

	@Override
	public String update(String json) {
		try {
			this.updateOperation = true;
			
			this.result = null;
			readItem();
			
			ArrayNode originalResourcesarrayNode = (ArrayNode) result.get(RESOURCES_KEY);
			for(JsonNode resourceNode : originalResourcesarrayNode) {
				GRSFResource grsfResource = new GRSFResource(itemID);
				grsfResource.setName(name);
				grsfResource.setPreviousRepresentation(resourceNode);
				grsfResource.deleteFile();
			}
			
			JsonNode jNode = objectMapper.readTree(json);
			elaborate(jNode);
			
			String jsonString = objectMapper.writeValueAsString(jsonNode);
			sendPostRequest(ITEM_UPDATE, jsonString);

			parseResult();
			
			return getAsCleanedString(result);

		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}

	@Override
	public String patch(String json) {
		try {
			this.patch = true;
			JsonNode jsonNode = objectMapper.readTree(json);
			elaborate(jsonNode);
			// TODO
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
		return null;
	}
	
	@Override
	public void purge() {
		try {
			setApiKey(CKANUtility.getSysAdminAPI());
			
			readItem();
			
			if(ckanUser.getRole().ordinal() < Role.ADMIN.ordinal() && !isItemCreator()) {
				throw new ForbiddenException("Only " + Role.ADMIN.getPortalRole() + "s and item creator are entitled to purge an item");
			}
			
			if(result.has(RESOURCES_KEY)) {
				itemID = result.get(ID_KEY).asText();
				ArrayNode arrayNode = (ArrayNode) result.get(RESOURCES_KEY);
				for(JsonNode jsonNode : arrayNode) {
					GRSFResource grsfResource = new GRSFResource(itemID);
					grsfResource.setName(name);
					grsfResource.setPreviousRepresentation(jsonNode);
					grsfResource.deleteFile(); // Only delete file is required because the item will be purged at the end
				}
			}
			
			sendPostRequest(PURGE, createJsonNodeWithNameAsID());
			
			postItemDeleted();
			
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
}
