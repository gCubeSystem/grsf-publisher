package org.gcube.grsf.publisher.ckan.others;

import java.net.URL;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.gcat.persistence.ckan.CKANResource;
import org.gcube.grsf.publisher.ckan.record.Record;
import org.gcube.grsf.publisher.workspace.GRSFStorageHubManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GRSFResource extends CKANResource {

	private static final Logger logger = LoggerFactory.getLogger(GRSFResource.class);

	public GRSFResource(String itemID) {
		super(itemID);
		this.storageHubManagement = new GRSFStorageHubManagement();
	}
	
	@Override
	public void deleteFile() {
		try {
			getPreviousRepresentation();
			URL url = new URL(previousRepresentation.get(URL_KEY).asText());
			mimeType = previousRepresentation.get(MIME_TYPE_KEY).asText();
			String filename = previousRepresentation.get(Record.NAME_PROPERTY).asText();
			filename = filename + Record.TIMESERIES_RESOURCE_FILE_EXTENSION;
			deleteStorageResource(url, filename, mimeType);
		} catch(Exception e) {
			logger.error("Unable to delete resource {}",
					previousRepresentation != null ? getAsString(previousRepresentation) : "", e);
		}
	}
	
	@Override
	protected void deleteStorageResource(URL url, String filename, String mimetype) {
		persistedURL = url;
		URL finalURL = getFinalURL(url);
		if(isStorageFile(finalURL)) {
			try {
				// String storageFilename = Record.getFilename(name, filename);
				((GRSFStorageHubManagement) storageHubManagement).setGrsfUUID(name);
				// storageHubManagement.deleteResourcePersistence(itemID, storageFilename, mimetype);
				storageHubManagement.deleteResourcePersistence(finalURL, itemID);
			} catch(Exception e) {
				throw new InternalServerErrorException(e);
			}
		}
	}
	
}
