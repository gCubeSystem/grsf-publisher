package org.gcube.grsf.publisher.ckan.record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TraceabilityUnitRecord extends Record {

	@Override
	public String getType() {
		return "Traceability Unit";
	}

}
