package org.gcube.grsf.publisher.ckan.record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FisheryRecord extends Record {

	@Override
	public String getType() {
		return "Fishery";
	}
	
}
