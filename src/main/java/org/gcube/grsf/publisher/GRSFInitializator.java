package org.gcube.grsf.publisher;

import java.io.IOException;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.grsf.publisher.configuration.isproxies.impl.GRSFGCoreISConfigurationProxyFactory;
import org.gcube.grsf.publisher.rest.RequestFilter;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFInitializator implements ApplicationManager {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(GRSFInitializator.class);
	
	public static final String NAME = "GRSF Publisher";
	
	public static boolean initialised;
	
	
	/** 
	 * {@inheritDoc}
	 * The method discover the plugins available on classpath and their own 
	 * supported capabilities and publish a ServiceEndpoint with the 
	 * discovered information.
	 * Furthermore create/connect to DB
	 */
	@Override
	public void onInit() {
		RequestFilter requestFilter = new RequestFilter();
		try {
			requestFilter.filter(null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		String context = SecretManagerProvider.instance.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "{} is Starting on context {}\n"
				+ "-------------------------------------------------------",
				NAME, context);
		
		ApplicationContext applicationContext = ContextProvider.get();
//		String grsfEServiceID  = applicationContext.id();

		
		try {
//			GRSFFacetBasedISConfigurationProxyFactory fbigcpf = new GRSFFacetBasedISConfigurationProxyFactory();
//			GRSFFacetBasedISConfigurationProxy facetBasedISConfigurationProxy = null;
			if(!initialised) {
//				CatalogueConfigurationFactory.addISConfigurationProxyFactory(fbigcpf);
				GRSFGCoreISConfigurationProxyFactory gcigcpf = new GRSFGCoreISConfigurationProxyFactory();
				CatalogueConfigurationFactory.addISConfigurationProxyFactory(gcigcpf);
//				facetBasedISConfigurationProxy = fbigcpf.getInstance(context);
//				facetBasedISConfigurationProxy.installQueryTemplate();
				initialised = true;
			}
//			else {
//				facetBasedISConfigurationProxy = fbigcpf.getInstance(context);
//			}
//			facetBasedISConfigurationProxy.setServiceEServiceID(grsfEServiceID);
//			facetBasedISConfigurationProxy.createCallsForToVirtualService();
		}catch (Exception e) {
//			logger.warn("{} is not configured through the Facet Based IS in context {}. The reason is: {}. Please create/addToContext the expected resources ASAP. The Gcore IS will be used.", NAME, context, e.getMessage());
//			logger.trace("{} is not configured through the Facet Based IS in context {}.\n", NAME, context, e);
		}
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "{} Started Successfully on context {}\n"
				+ "-------------------------------------------------------",
				NAME, context);
		
	}
	
	/** 
	 * {@inheritDoc} 
	 * This function is invoked before the service will stop and unpublish the 
	 * resource from the IS to maintain the infrastructure integrity.
	 * Furthermore close the connection to DB.
	 */
	@Override
	public void onShutdown(){
		RequestFilter requestFilter = new RequestFilter();
		try {
			requestFilter.filter(null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		String context = SecretManagerProvider.instance.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "{} is Stopping on context {}\n"
				+ "-------------------------------------------------------", 
				NAME, context);
		
		/*
		 * We don't delete the relation.
		 * Thanks to the propagation constraint, 
		 * it will be deleted when the EService will be deleted.
		 *  
		ApplicationContext applicationContext = ContextProvider.get();
		String grsfEServiceID  = applicationContext.id();
		*/
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "{} Stopped Successfully on context {}\n"
				+ "-------------------------------------------------------", 
				NAME, context);
	}
}
