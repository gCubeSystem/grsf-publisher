package org.gcube.grsf.publisher.enums;

import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonCreator;
import org.gcube.com.fasterxml.jackson.annotation.JsonValue;

/**
 * Source Group and sub groups (for both StockRESTAPIs and FisheryRESTAPIs) -> look at "Database Source"
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public enum Source {

	FIRMS("FIRMS","firms"),
	RAM("RAM","ram"),
	FISHSOURCE("FishSource", "fishsource"),
	GRSF("GRSF", "grsf"),
	SDG("FAO SDG 14.4.1 Questionnaire","sdg");

	private String sourceName;
	private String urlPath;

	private Source(String sourceName, String urlPath) {
		this.sourceName = sourceName;
		this.urlPath = urlPath;
	}

	/**
	 * Return the original name
	 * @return
	 */
	public String getSourceName(){
		return sourceName;
	}

	@JsonValue
	public String onSerialize(){
		return urlPath;
	}

	@JsonCreator
	public static Source onDeserialize(String sourceString) {
		if(sourceString != null) {
			for(Source source : Source.values()) {
				if (source.urlPath.equalsIgnoreCase(sourceString.trim())) {
					return source;
				}
				if (source.sourceName.equalsIgnoreCase(sourceString.trim())) {
					return source;
				}
			}
		}
		return null;
	}
	
	public String getURLPath() {
		return urlPath;
	}
	
	@Override
	public String toString() {
		return urlPath;
	}
	
	public static String getJsonArrayAsString(){
		return "[" + FIRMS.urlPath  + "," + 
				RAM.urlPath + "," +  
				FISHSOURCE.urlPath + "," + 
				GRSF.urlPath + "," +  
				SDG.urlPath + "," + "]";
	}
	
	public static List<String> listNames(){
		
		List<String> valuesString = new ArrayList<String>(Source.values().length);
		for(Source source : Source.values()) {
			valuesString.add(source.getSourceName());
		}
		
		return valuesString;
	}

}
