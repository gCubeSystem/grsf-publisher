package org.gcube.grsf.publisher.workspace;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.gcat.workspace.GcatStorageHubManagement;
import org.gcube.grsf.publisher.ckan.record.Record;
import org.gcube.storagehub.MetadataMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GRSFStorageHubManagement extends GcatStorageHubManagement {

	private final Logger logger = LoggerFactory.getLogger(GRSFStorageHubManagement.class);
	
	protected String grsfUUID;
	
	protected URL url;
	protected FileContainer fileContainer;
	
	public GRSFStorageHubManagement() {
		super();
	}
	
	public void setGrsfUUID(String grsfUUID) {
		this.grsfUUID = grsfUUID;
	}
	
	public URL getPublicLink() {
		return url;
	}

	public FileContainer getFileContainer() {
		return fileContainer;
	}
	
	protected MetadataMatcher getMetadataMatcher() {
		MetadataMatcher metadataMatcher = new GRSFMetadataMatcher(grsfUUID);
		return metadataMatcher;
	}
	
	/**
	 * Save the file in the workspace using storagehub-application-persistence
	 * @param file the file to persist in the workspace
	 * @return 
	 * @throws Exception 
	 */
	public void persistFile(File file) throws Exception{
		FileInputStream fis = new FileInputStream(file);
		storageHubManagement.setMetadataMatcher(getMetadataMatcher());
		url = storageHubManagement.persistFile(fis, file.getName(), Record.TIMESERIES_MIMETYPE);
		fileContainer = storageHubManagement.getPersistedFile();
		logger.info("File '{}' has been persisted in StorageHub with ID '{}'. File public Link is {}", file.getName(), fileContainer.getId(), url);
	}
}
