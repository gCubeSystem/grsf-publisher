package org.gcube.grsf.publisher.workspace;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.grsf.publisher.GRSFInitializator;
import org.gcube.grsf.publisher.ckan.record.Record;
import org.gcube.storagehub.MetadataMatcher;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GRSFMetadataMatcher extends MetadataMatcher {
	
	public static final String GRSF_METADATA_VERSION = "1.0.0";
	
	public GRSFMetadataMatcher(String grsfUUID) {
		super(GRSFInitializator.NAME, GRSF_METADATA_VERSION, grsfUUID);
	}
	
	@Override
	public boolean check(Metadata metadata) {
		Map<String,Object> map = metadata.getMap();
		Object obj = map.get(Record.GRSF_UUID_PROPERTY);
		if(obj!=null && obj.toString().compareTo(id) == 0) {
			return true;
		}
		return false;
	}
	
	@Override
	protected Map<String, Object> getSpecificMetadataMap() {
		Map<String,Object> map = new HashMap<>();
		map.put(Record.GRSF_UUID_PROPERTY, id);
		return map;
	}
	
}
